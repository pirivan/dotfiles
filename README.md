---
title: Dotfiles Repository
---

This repository hosts the dotfiles I use in my Linux and MacOS laptops and computers.

# Requirements

You must install [chezmoi](https://www.chezmoi.io/install/) as I use it to manage my dotfiles.

For the MacOS setup you must also install [homebrew](https://brew.sh/) in your laptop.
You can use it to install `chezmoi` using the command `brew install chezmoi`.

I use the `zsh` shell with the [oh-my-zsh](https://ohmyz.sh/) scripts by default. While not a strict requirement, using `zsh` as your login and interactive shell is convenient when using this repository. 

# How I use it

The steps below show how I use this repository.

1. Tell `chezmoi` to use this repository:
   
   ```sh
   chezmoi init https://gitlab.com/pirivan/dotfiles
   ```
   
   <mark>Note</mark>: while developing new features I use the following command instead:
   
   ```sh
   chezmoi init --branch develop https://gitlab.com/pirivan/dotfiles
   ```

2. Verify the differences with your current setup:
   
   ```sh
   chezmoi diff
   ```
   
   Backup any configuration files if required.

3. Apply the changes:
   
   ```sh
   chezmoi apply
   ```

4. Start new shell sessions to make the dotfiles effective. 

5. To update my local setup to incorporate changes from this repository:
   
   ```shell
   chezmoi update
   chezmoi diff
   chezmoi apply
   ```

## Architecture

When you run the `chezmoi init` command, the logic in the `home/.chezmoi.yaml.tmpl` template determines the **deployment** type, to which it assigns a name (currently `pmac-01` and `pmac-02` to indicate "personal" mac laptops). A deployment uniquely identifies the computer, OS, shell, user, and other attributes. To see the deployment details use the command `chezmoi data` and look at the `data` map.

Once the deployment is identified, all other installation templates and configuration files use the deployment name, or some of its attributes, to determine what to do when you run `chezmoi apply`.

Details about my definition of a deployment are available in the issue #2 in this repo.

<mark>Note</mark>: When running `chezmoi init` on an unknown environment, like in a new laptop, the deployment name becomes "unknown". The unknown deployment does nothing when you run `chezmoi apply`. You can use this fact to get started with a fork of this repository before you customize it to define your own deployment types.

# Features

Running `chezmoi apply` installs/updates several packages, configures dotfiles, and sets up suitable shell aliases. The following CLI utilities are included:

<mark>Note</mark>: Package names followed by a `*` are installed only on my personal computers.

| Package                                                                  | Comment                                               |
| ------------------------------------------------------------------------ | ----------------------------------------------------- |
| [bat](https://github.com/sharkdp/bat)                                    | A supercharged `cat`                                  |
| [bitwarden-cli](https://bitwarden.com/help/cli/) `*`                     | The Bitwarden command-line interface                  |
| [broot](https://dystroy.org/broot/)                                      | A fast file browser                                   |
| [curl](https://curl.se/)                                                 | A tool to transfer data with URLs                     |
| [duf](https://github.com/muesli/duf)                                     | Disk Usage/Free Utility, an alternative to `df`       |
| [exa](https://the.exa.website/)                                          | Alternative to `ls`                                   |
| [fd](https://github.com/sharkdp/fd)                                      | Fast alternative to `find`                            |
| [fzf](https://github.com/junegunn/fzf)                                   | A general-purpose command-line fuzzy finder           |
| [gh](https://cli.github.com/)                                            | The GitHub CLI                                        |
| [git](https://git-scm.com/)                                              | The version control system                            |
| [git-delta](https://github.com/dandavison/delta)                         | A syntax-highlighting pager for git, diff, and grep   |
| [glances](https://github.com/nicolargo/glances)                          | A laptop monitoring tool                              |
| [go](https://go.dev/)                                                    | The Go programming language                           |
| [golangci-lint](https://golangci-lint.run/)                              | A Go linters aggregator                               |
| [gping](https://github.com/orf/gping)                                    | A pingcommand replacement                             |
| [jq](https://stedolan.github.io/jq/)                                     | `sed` for JSON data                                   |
| [kitty](https://github.com/kovidgoyal/kitty)                             | A lightweight terminal emulator                       |
| [micro](https://micro-editor.github.io/)                                 | Because I don't like `vi`! (2)                        |
| [neofetch](https://github.com/dylanaraps/neofetch)                       | A command-line system information tool                |
| [nmap](https://nmap.org/)                                                | A utility for network discovery and security auditing |
| [pandoc](https://pandoc.org/)                                            | A file type converter                                 |
| [pyenv](https://github.com/pyenv/pyenv)                                  | A Python version management system                    |
| [ripgrep](https://github.com/BurntSushi/ripgrep)                         | A replacement for `grep`                              |
| [speedcrunch](https://heldercorreia.bitbucket.io/speedcrunch/index.html) | A high-precision scientific calculator                |
| [tokei](https://github.com/XAMPPRocky/tokei)                             | Displays code statistics                              |
| [watch](https://www.man7.org/linux/man-pages/man1/watch.1.html)          | The Linux `watch` command                             |
| [zoxide](https://github.com/ajeetdsouza/zoxide)                          | A smarter `cd` command                                |

The following desktop applications are included:

| Package                                          | Comment                                     |
| ------------------------------------------------ | ------------------------------------------- |
| [balenaEtcher](https://www.balena.io/etcher/)    | Flash OS images to SD cards and USB drives  |
| [logseq](https://logseq.com/)                    | A privacy-first, open-source knowledge base |
| [marktext](https://github.com/marktext/marktext) | A simple and elegant markdown editor        |
| [pycharm-ce](https://www.jetbrains.com/pycharm/) | The Python IDE, community edition           |
| [signal](https://signal.org/) `*`                | The Signal messanger app                    |
| [telegram](https://telegram.org/) `*`            | The Telegram messanger app                  |
| [vsc](https://code.visualstudio.com/)            | The Visual Studio Code editor               |
| [zotero](zotero.org)                             | Personal research assistant                 |

# Configuration notes

The following sections cover package installation details you may need after running the `chezmoi apply` command.

## Controlled files

Use the command `chezmoi list` to get a list of dotfiles under the control of `chezmoi`. Here is an example:

```shell
% chezmoi list
.config
.config/kitty
.config/kitty/kitty.conf
.config/micro
.config/micro/bindings.json
.config/micro/settings.json
.digrc
.gitconfig
.zlogin
.zshrc
```

You must ensure to update `chezmoi` if you make any changes to these files (use the command `chezmoi add`).

## Pycharm

You need to configure `pycharm` to use the python interpreter provided by `pyenv`. Open the editor settings (`cmd-,` on a Mac) --> `Python Interpreter` --> `Add interpreter` --> `Add local interpreter`. Select `Virtual Environment`, `Existing environment`, and then use the three-dots menu to select the proper pyenv shim as illustrated below.

![pycharm-pyenv.png](./assets/pycharm-pyenv.png)

Once you add the interpreter, go back to `Python Interpreter` and select the interpreter you just added. Press `OK` to save the selection.

![pycharm-interpreter.png](./assets/pycharm-interpreter.png)

# Credits

This repo borrows several ideas from Tom Payne's [dotfiles](https://github.com/twpayne/dotfiles/) repository.
